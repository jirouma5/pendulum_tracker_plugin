#include <pendulum_tracker.h>

namespace pendulum_tracker_plugin
{

namespace pendulum_tracker
{

using namespace Eigen;

//}

// | -------------- tracker's interface routines -------------- |

/* //{ initialize() */

bool PendulumTracker::initialize(const ros::NodeHandle &nh, std::shared_ptr<mrs_uav_managers::control_manager::CommonHandlers_t> common_handlers,
                                std::shared_ptr<mrs_uav_managers::control_manager::PrivateHandlers_t> private_handlers) {

  this->common_handlers_  = common_handlers;
  this->private_handlers_ = private_handlers;

  _uav_name_ = common_handlers->uav_name;

  nh_ = nh;

  ros::Time::waitForValid();

  last_update_time_ = ros::Time(0);

  // --------------------------------------------------------------
  // |                     loading parameters                     |
  // --------------------------------------------------------------

  // | -------------------- load param files -------------------- |

  bool success = true;

  // FYI
  // This method will load the file using `rosparam get`
  //   Pros: you can the full power of the official param loading
  //   Cons: it is slower
  //
  // Alternatives:
  //   You can load the file directly into the ParamLoader as shown below.

  // success *= private_handlers->loadConfigFile(ros::package::getPath("pendulum_tracker_plugin") + "/config/pendulum_tracker.yaml");
  success *= private_handlers->loadConfigFile(ros::package::getPath("pendulum_tracker_plugin") + "/config/pendulum_tracker_square.yaml");

  if (!success) {
    return false;
  }

  // | ---------------- load plugin's parameters ---------------- |

  mrs_lib::ParamLoader param_loader(nh_, "PendulumTracker");

  // This is the alternaive way of loading the config file.
  //
  // Files loaded using this method are prioritized over ROS params.
  //
  // param_loader.addYamlFile(ros::package::getPath("pendulum_tracker_plugin") + "/config/pendulum_tracker.yaml");

  // param_loader.loadParam("preview", drs_params_.preview);
  loadParams(param_loader);


  if (!param_loader.loadedSuccessfully()) {
    ROS_ERROR("[PendulumTracker]: could not load all parameters!");
    return false;
  }

  // | --------------- dynamic reconfigure server --------------- |

  drs_.reset(new Drs_t(mutex_drs_, nh_));
  drs_->updateConfig(drs_params_);
  Drs_t::CallbackType f = boost::bind(&PendulumTracker::callbackDrs, this, _1, _2);
  drs_->setCallback(f);

  tracker = std::make_shared<MpcTracker>(model_);
  // trackerExtended = std::make_shared<MpcTrackerExtended>(model_);

  // | ----------------------- subscribers ---------------------- |

  mrs_lib::SubscribeHandlerOptions shopts;
  shopts.nh                 = nh_;
  shopts.node_name          = "PendulumTracker";
  shopts.no_message_timeout = mrs_lib::no_timeout;
  shopts.threadsafe         = true;
  shopts.autostart          = true;
  shopts.queue_size         = 10;
  shopts.transport_hints    = ros::TransportHints().tcpNoDelay();

  sh_state_ = mrs_lib::SubscribeHandler<pendulum_controller_plugin::CompleteState>(shopts, "/" + common_handlers_->uav_name + "/control_manager/pendulum_controller/state_est");

  // | ----------------------- publishers ----------------------- |

  ph_trajViz_      = mrs_lib::PublisherHandler<geometry_msgs::PoseArray>(nh_, "trajectory_vis", 10, true);
  ph_traj_         = mrs_lib::PublisherHandler<pendulum_controller_plugin::Trajectory>(nh_, "trajectory", 10, true);

  // | --------------------- finish the init -------------------- |

  is_initialized_ = true;

  ROS_INFO("[PendulumTracker]: initialized");

  return true;
}

//}

/* //{ activate() */

std::tuple<bool, std::string> PendulumTracker::activate([[maybe_unused]] const std::optional<mrs_msgs::TrackerCommand> &last_tracker_cmd) {

  if (last_tracker_cmd) {

    std::scoped_lock lock(mutex_goal_);

    goal_x_       = last_tracker_cmd->position.x;
    goal_y_       = last_tracker_cmd->position.y;
    goal_z_       = last_tracker_cmd->position.z;
    goal_heading_ = last_tracker_cmd->heading;

  } else {

    auto uav_state = mrs_lib::get_mutexed(mutex_uav_state_, uav_state_);

    goal_x_ = uav_state.pose.position.x;
    goal_y_ = uav_state.pose.position.y;
    goal_z_ = uav_state.pose.position.z;

    try {
      goal_heading_ = mrs_lib::AttitudeConverter(uav_state.pose.orientation).getHeading();
    }
    catch (...) {
      goal_heading_ = 0;
    }

    updateMpcParams();
    updateMpcState(uav_state);

    // prepare the reference
    auto params = mrs_lib::get_mutexed(mutex_drs_params_, drs_params_);
    Vector3d refPos(uav_state.pose.position.x, uav_state.pose.position.y, uav_state.pose.position.z);
    MatrixXd refPosStamped = MatrixXd::Zero(1,4);
    double timestamp = ros::Time::now().toSec() + params.preview;
    refPosStamped << timestamp, refPos;

    std::tie(traj, ctrl) = tracker->setReferencePoint(refPos, timestamp);
    timeRefSet = ros::Time::now();
  }

  std::stringstream ss;
  ss << "activated";
  ROS_INFO_STREAM("[PendulumTracker]: " << ss.str());

  is_active_ = true;

  return std::tuple(true, ss.str());
}

//}

/* //{ deactivate() */

void PendulumTracker::deactivate(void) {

  is_active_ = false;

  ROS_INFO("[PendulumTracker]: deactivated");
}

//}

/* //{ update() */

std::optional<mrs_msgs::TrackerCommand> PendulumTracker::update(const mrs_msgs::UavState &                                          uav_state,
                                                               [[maybe_unused]] const mrs_uav_managers::Controller::ControlOutput &last_control_output) {

  {
    std::scoped_lock lock(mutex_uav_state_);

    uav_state_ = uav_state;

    got_uav_state_ = true;
  }

  mrs_lib::set_mutexed(mutex_world_frame_id_, uav_state.header.frame_id, world_frame_id_);

  // | ---------- calculate dt from the last iteration ---------- |

  double dt;

  if (first_iteration_) {
    dt               = 0.01;
    first_iteration_ = false;
  } else {
    dt = (uav_state.header.stamp - last_update_time_).toSec();
  }

  // updateI++;

  // if (updateI == 100){
  //   updateI = 0;
  //   std::cout << "update\n";
  //   if (trajTracking){

  //   }
  // }

  last_update_time_ = uav_state.header.stamp;

  if (fabs(dt) < 0.001) {

    ROS_DEBUG("[PendulumTracker]: the last odometry message came too close (%.2f s)!", dt);
    dt = 0.01;
  }

  // update mass
  totalMass = last_control_output.diagnostics.total_mass;

  // up to this part the update() method is evaluated even when the tracker is not active
  if (!is_active_) {
    return {};
  }

  // | ------------------- fill in the result ------------------- |

  mrs_msgs::TrackerCommand tracker_cmd;

  tracker_cmd.header.stamp    = ros::Time::now();
  tracker_cmd.header.frame_id = uav_state.header.frame_id;

  mrs_msgs::MpcPredictionFullState trajectory;
  trajectory.header.stamp = ros::Time::now();
  trajectory.header.frame_id = uav_state.header.frame_id;

  {
    std::scoped_lock lock(mutex_goal_);

    for (int i = 0; i < traj.rows(); i++){

      // time
      auto t = timeRefSet + ros::Duration(i*TG_TS);
      trajectory.stamps.push_back(t);

      // position
      geometry_msgs::Point position;
      position.x = traj(i, 0);
      position.y = traj(i, 1);
      position.z = traj(i, 2);

      trajectory.position.push_back(position);

      // velocity
      geometry_msgs::Vector3 velocity;
      velocity.x = traj(i, 5);
      velocity.y = traj(i, 6);
      velocity.z = traj(i, 7);

      trajectory.velocity.push_back(velocity);
    }

    tracker_cmd.position.x = goal_x_;
    tracker_cmd.position.y = goal_y_;
    tracker_cmd.position.z = goal_z_;
    tracker_cmd.heading    = goal_heading_;

    tracker_cmd.header.seq = goal_idx_;
  }

  tracker_cmd.full_state_prediction = trajectory;

  tracker_cmd.use_position_vertical   = 1;
  tracker_cmd.use_position_horizontal = 1;
  tracker_cmd.use_heading             = 1;

  return {tracker_cmd};
}

//}

/* //{ resetStatic() */

bool PendulumTracker::resetStatic(void) {

  return false;
}

//}

/* //{ getStatus() */

const mrs_msgs::TrackerStatus PendulumTracker::getStatus() {

  mrs_msgs::TrackerStatus tracker_status;

  tracker_status.active            = is_active_;
  tracker_status.callbacks_enabled = callbacks_enabled_;

  return tracker_status;
}

//}

/* //{ enableCallbacks() */

const std_srvs::SetBoolResponse::ConstPtr PendulumTracker::enableCallbacks(const std_srvs::SetBoolRequest::ConstPtr &cmd) {

  std_srvs::SetBoolResponse res;
  std::stringstream         ss;

  if (cmd->data != callbacks_enabled_) {

    callbacks_enabled_ = cmd->data;

    ss << "callbacks " << (callbacks_enabled_ ? "enabled" : "disabled");
    ROS_INFO_STREAM_THROTTLE(1.0, "[PendulumTracker]: " << ss.str());

  } else {

    ss << "callbacks were already " << (callbacks_enabled_ ? "enabled" : "disabled");
    ROS_WARN_STREAM_THROTTLE(1.0, "[PendulumTracker]: " << ss.str());
  }

  res.message = ss.str();
  res.success = true;

  return std_srvs::SetBoolResponse::ConstPtr(new std_srvs::SetBoolResponse(res));
}

//}

/* switchOdometrySource() //{ */

const std_srvs::TriggerResponse::ConstPtr PendulumTracker::switchOdometrySource([[maybe_unused]] const mrs_msgs::UavState &new_uav_state) {

  return std_srvs::TriggerResponse::Ptr();
}

//}

/* //{ hover() */

const std_srvs::TriggerResponse::ConstPtr PendulumTracker::hover([[maybe_unused]] const std_srvs::TriggerRequest::ConstPtr &cmd) {

  return std_srvs::TriggerResponse::Ptr();
}

//}

/* //{ startTrajectoryTracking() */

const std_srvs::TriggerResponse::ConstPtr PendulumTracker::startTrajectoryTracking([[maybe_unused]] const std_srvs::TriggerRequest::ConstPtr &cmd) {

  trajTracking = true;

  updateMpcParams();
  updateMpcState();

  std::tie(traj, ctrl) = tracker->setReferenceTrajectory(trajRef);
  timeRefSet = ros::Time::now();

  visualizeTrajectory(traj);

  goal_idx_++;

  std_srvs::TriggerResponse response;
  response.message = "reference set";
  response.success = true;
  return std_srvs::TriggerResponse::ConstPtr(new std_srvs::TriggerResponse(response));
}

//}

/* //{ stopTrajectoryTracking() */

const std_srvs::TriggerResponse::ConstPtr PendulumTracker::stopTrajectoryTracking([[maybe_unused]] const std_srvs::TriggerRequest::ConstPtr &cmd) {
  trajTracking = false;
  return std_srvs::TriggerResponse::Ptr();
}

//}

/* //{ resumeTrajectoryTracking() */

const std_srvs::TriggerResponse::ConstPtr PendulumTracker::resumeTrajectoryTracking([[maybe_unused]] const std_srvs::TriggerRequest::ConstPtr &cmd) {
  return startTrajectoryTracking(std_srvs::TriggerRequest::ConstPtr(new std_srvs::TriggerRequest()));;
}

//}

/* //{ gotoTrajectoryStart() */

const std_srvs::TriggerResponse::ConstPtr PendulumTracker::gotoTrajectoryStart([[maybe_unused]] const std_srvs::TriggerRequest::ConstPtr &cmd) {
  return std_srvs::TriggerResponse::Ptr();
}

//}

/* //{ setConstraints() */

const mrs_msgs::DynamicsConstraintsSrvResponse::ConstPtr PendulumTracker::setConstraints([
    [maybe_unused]] const mrs_msgs::DynamicsConstraintsSrvRequest::ConstPtr &cmd) {

  {
    std::scoped_lock lock(mutex_constraints_);

    constraints_ = cmd->constraints;
  }

  mrs_msgs::DynamicsConstraintsSrvResponse res;

  res.success = true;
  res.message = "constraints updated";

  return mrs_msgs::DynamicsConstraintsSrvResponse::ConstPtr(new mrs_msgs::DynamicsConstraintsSrvResponse(res));
}

//}

/* //{ setReference() */

const mrs_msgs::ReferenceSrvResponse::ConstPtr PendulumTracker::setReference([[maybe_unused]] const mrs_msgs::ReferenceSrvRequest::ConstPtr &cmd) {

  updateMpcParams();
  updateMpcState();

  // prepare the reference
  auto params = mrs_lib::get_mutexed(mutex_drs_params_, drs_params_);
  Vector3d refPos(cmd->reference.position.x, cmd->reference.position.y, cmd->reference.position.z);
  MatrixXd refPosStamped = MatrixXd::Zero(1,4);
  double timestamp = ros::Time::now().toSec() + params.preview;
  refPosStamped << timestamp, refPos;

  std::tie(traj, ctrlDif) = tracker->setReferencePoint(refPos, timestamp);
  timeRefSet = ros::Time::now();

  // std::cout << "trajectory:\n" << traj << "\n";
  // std::cout << "control:\n" << ctrl << "\n";
  // auto ctrlDif = ctrl.block(1,0, ctrl.rows() - 1, ctrl.cols()) - ctrl.block(0, 0, ctrl.rows() - 1, ctrl.cols());
  
  std::cout << "control difference:\n" << ctrlDif << "\n";

  // std::cout << "row 1 norm:\n" << ctrlDif.row(0).norm() << "\n";

  double ctrlThres = 0.001;
  bool blockStarted = false;
  std::vector<int> blockStartI;
  std::vector<int> blockEndI;

  for (int i = 0; i < ctrlDif.rows(); i++){
    if (ctrlDif.row(i).norm() < ctrlThres){
      if (!blockStarted){
        blockStartI.push_back(i);
        blockStarted = true;
      }
    } else {
      if (blockStarted){
        blockEndI.push_back(i);
        blockStarted = false;
      }
    }
  }

  std::cout << "Block starts:";
  for (auto i : blockStartI){
    std::cout << ", " << i;
  }
  std::cout << "\nBlock ends:";
  for (auto i : blockEndI){
    std::cout << ", " << i;
  }
  std::cout << "\n";

  // todo set blocking and extend the horizon


  visualizeTrajectory(traj);

  goal_x_       = cmd->reference.position.x;
  goal_y_       = cmd->reference.position.y;
  goal_z_       = cmd->reference.position.z;
  goal_heading_ = cmd->reference.heading;

  goal_idx_++;

  mrs_msgs::ReferenceSrvResponse response;

  response.message = "reference set";
  response.success = true;

  return mrs_msgs::ReferenceSrvResponse::ConstPtr(new mrs_msgs::ReferenceSrvResponse(response));
}

//}

/* //{ setVelocityReference() */

const mrs_msgs::VelocityReferenceSrvResponse::ConstPtr PendulumTracker::setVelocityReference([
    [maybe_unused]] const mrs_msgs::VelocityReferenceSrvRequest::ConstPtr &cmd) {
  return mrs_msgs::VelocityReferenceSrvResponse::Ptr();
}

//}

/* //{ setTrajectoryReference() */

const mrs_msgs::TrajectoryReferenceSrvResponse::ConstPtr PendulumTracker::setTrajectoryReference([
    [maybe_unused]] const mrs_msgs::TrajectoryReferenceSrvRequest::ConstPtr &cmd) {

  trajRef = cmd->trajectory;

  if (cmd->trajectory.fly_now){

    startTrajectoryTracking(std_srvs::TriggerRequest::ConstPtr(new std_srvs::TriggerRequest()));
  }

  mrs_msgs::TrajectoryReferenceSrvResponse response;
  response.message = "reference set";
  response.success = true;
  return mrs_msgs::TrajectoryReferenceSrvResponse::ConstPtr(new mrs_msgs::TrajectoryReferenceSrvResponse(response));
}

//}

// --------------------------------------------------------------
// |                          callbacks                         |
// --------------------------------------------------------------

/* //{ callbackDrs() */

void PendulumTracker::callbackDrs(pendulum_tracker_plugin::pendulum_trackerConfig &config, [[maybe_unused]] uint32_t level) {

  mrs_lib::set_mutexed(mutex_drs_params_, config, drs_params_);

  ROS_INFO("[Pendulumtracker]: dynamic reconfigure params updated");
}

void PendulumTracker::loadParams(mrs_lib::ParamLoader &param_loader){
  param_loader.loadParam("preview", drs_params_.preview);
  param_loader.loadParam("interpolate", drs_params_.interpolate);

  // Tracker's cost function
  // States
  param_loader.loadParam("posHQ", drs_params_.posHQ);
  param_loader.loadParam("posVQ", drs_params_.posVQ);
  param_loader.loadParam("velHQ", drs_params_.velHQ);
  param_loader.loadParam("velVQ", drs_params_.velVQ);
  param_loader.loadParam("attitudeQ", drs_params_.attitudeQ);
  param_loader.loadParam("loadAmpQ", drs_params_.loadAmpQ);
  param_loader.loadParam("loadRateQ", drs_params_.loadRateQ);
  // Control actions
  param_loader.loadParam("attitudeCtrlR", drs_params_.attitudeCtrlR);
  param_loader.loadParam("forceCtrlR", drs_params_.forceCtrlR);

  // Tracker's constraints
  param_loader.loadParam("attitudeCtrlBound", drs_params_.attitudeCtrlBound);
  param_loader.loadParam("velHBound", drs_params_.velHBound);
  param_loader.loadParam("slackCost", drs_params_.slackCost);
}

void PendulumTracker::updateMpcParams(){
  // update params
  auto params = mrs_lib::get_mutexed(mutex_drs_params_, drs_params_);

  // set interpolation
  tracker->setParameter("interpolate", params.interpolate);

  // trajectory params
  MatrixXd Q = MatrixXd::Identity(N_STATES + N_INPUTS, N_STATES + N_INPUTS);
  // position of the uav
  Q.block(0, 0, 2, 2) *= params.posHQ;
  Q(2, 2) *= params.posVQ;
  // angle of the pendulum
  Q.block(3, 3, 2, 2) *= params.loadAmpQ;
  // velocity of the uav
  Q.block(5, 5, 2, 2) *= params.velHQ;
  Q(7, 7) *= params.velVQ;
  // angular rate of the pendulum
  Q.block(8, 8, 2, 2) *= params.loadRateQ;
  // attitude of the uav
  Q.block(10, 10, 2, 2) *= params.attitudeQ;

  // control actions
  Q.block(N_STATES - 1, N_STATES - 1, 2, 2) *= params.attitudeCtrlR;
  Q(N_STATES + N_INPUTS - 1, N_STATES + N_INPUTS - 1) *= params.forceCtrlR;

  tracker->setParameter("Q", Q);

  // MatrixXd R = Matrix3d::Identity();
  // R.block(0, 0, 2, 2) *= params.attitudeCtrlR;
  // R(2, 2) *= params.forceCtrlR;
  // tracker->setParameter("R", R);

  // constraints
  Vector3d bu = Vector3d(params.attitudeCtrlBound, params.attitudeCtrlBound, 5.0/13.0*model_.getParams().g*totalMass);
  tracker->setParameter("bu", bu);
  tracker->setParameter("Z", params.slackCost);
  tracker->setParameter("maxVelHrz", params.velHBound);

  return;
}

void PendulumTracker::updateMpcState(){
  if (sh_state_.hasMsg()){
    pendulum_controller_plugin::CompleteStateConstPtr uavState = sh_state_.getMsg();

    Eigen::VectorXd state = Eigen::VectorXd::Zero(N_STATES);

    state <<
      uavState->uav.position.x, uavState->uav.position.y, uavState->uav.position.z,
      uavState->pendulum.attitude.phi, uavState->pendulum.attitude.theta,
      uavState->uav.velocity.x, uavState->uav.velocity.y, uavState->uav.velocity.z,
      uavState->pendulum.attitudeRate.phi, uavState->pendulum.attitudeRate.theta,
      uavState->uav.attitude.pitch, uavState->uav.attitude.roll, uavState->uav.attitude.yaw
      ;

    tracker->updateState(state);
    
  }
}

void PendulumTracker::updateMpcState(const mrs_msgs::UavState & uavState){

  Eigen::VectorXd state = Eigen::VectorXd::Zero(N_STATES);
  state << uavState.pose.position.x, uavState.pose.position.y, uavState.pose.position.z;

  tracker->updateState(state);
}

void PendulumTracker::visualizeTrajectory(MatrixXd trajectoryIn){
  // visualize the trajectory

  auto world_frame_id = mrs_lib::get_mutexed(mutex_world_frame_id_, world_frame_id_);
  geometry_msgs::PoseArray poseArray;
  poseArray.header.frame_id = world_frame_id;
  poseArray.header.stamp    = ros::Time::now();

  pendulum_controller_plugin::Trajectory trajectory;

  auto timeStartTraj = ros::Time::now();
  for (int i = 0; i < trajectoryIn.rows(); i++){
    geometry_msgs::Pose pose;
    pose.position.x = trajectoryIn(i, 0);
    pose.position.y = trajectoryIn(i, 1);
    pose.position.z = trajectoryIn(i, 2);

    pose.orientation = mrs_lib::AttitudeConverter(trajectoryIn(i, 11), trajectoryIn(i, 10), 0);

    poseArray.poses.push_back(pose);

    pendulum_controller_plugin::CompleteState state;
    state.uav.position.x = trajectoryIn(i, 0);
    state.uav.position.y = trajectoryIn(i, 1);
    state.uav.position.z = trajectoryIn(i, 2);

    state.uav.velocity.x = trajectoryIn(i, 5);
    state.uav.velocity.y = trajectoryIn(i, 6);
    state.uav.velocity.z = trajectoryIn(i, 7);

    state.uav.attitude.pitch = trajectoryIn(i, 10);
    state.uav.attitude.roll = trajectoryIn(i, 11);
    state.uav.attitude.yaw = 0;
    // state.uav.attitude.f = trajectoryIn(i, 7);

    state.pendulum.attitude.phi = trajectoryIn(i, 3);
    state.pendulum.attitude.theta = trajectoryIn(i, 4);

    state.pendulum.attitudeRate.phi = trajectoryIn(i, 8);
    state.pendulum.attitudeRate.theta = trajectoryIn(i, 9);

    trajectory.states.push_back(state);

    auto tTg = timeStartTraj + ros::Duration(i*TG_TS);
    trajectory.stamps.push_back(tTg);
  }
  ph_trajViz_.publish(poseArray);
  ph_traj_.publish(trajectory);
}

//}

}  // namespace pendulum_tracker

}  // namespace pendulum_tracker_plugin

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(pendulum_tracker_plugin::pendulum_tracker::PendulumTracker, mrs_uav_managers::Tracker)
