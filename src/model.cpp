#include <model.h>

#include <iostream>

namespace pendulum_tracker_plugin
{

namespace pendulum_tracker
{

using namespace Eigen;

std::tuple<MatrixXd, MatrixXd, MatrixXd> ModelUavPend::getLinModel(double Ts){

	// state matrix A
	MatrixXd A = MatrixXd::Zero(N_STATES, N_STATES);
	A.block(0, 5, 5, 5) = MatrixXd::Identity(5, 5);

	A.block(5, 4, 1, 6) << -p.g*p.m_l/p.m_c, 0, 0, 0, 0, -p.d/(p.l*p.m_c);
	A.block(6, 3, 1, 6) = -A.block(5, 4, 1, 6);

	A.block(5, 10, 2, 2) = (p.m_l*p.g/p.m_c + p.g) * MatrixXd::Identity(2, 2);
	A(6, 11) *= -1;

	A.block(8, 3, 2, 2) = -p.g*(p.m_c+p.m_l)/(p.l*p.m_c) * MatrixXd::Identity(2, 2);
	A.block(8, 8, 2, 2) = -p.d*(p.m_c+p.m_l)/(p.l*p.l*p.m_c*p.m_l) * MatrixXd::Identity(2, 2);

	A(8, 11) = p.g*(p.m_c + p.m_l)/(p.l*p.m_c);
	A(9, 10) = A(8, 11);

	A(10, 10) = -1/p.tau1;
	A(11, 11) = -1/p.tau2;

	// input matrix B
	MatrixXd B = MatrixXd::Zero(N_STATES, N_INPUTS);
	B(7, 2) = 1/(p.m_c + p.m_l);
	B(10, 0) = p.K1/p.tau1;
	B(11, 1) = p.K2/p.tau2;

	// output matrix C
	MatrixXd C = MatrixXd::Zero(N_OUTPUTS, N_STATES);
	C.block(0, 0, 3, 3) = MatrixXd::Identity(3, 3);
	C.block(3, 10, 2, 2) = MatrixXd::Identity(2, 2);

	if (Ts <= 0){
		return {A, B, C};
	}

	// approximation of Tustin discretization method
	A = MatrixXd::Identity(N_STATES, N_STATES) + Ts*A;
	B *= Ts;

	return {A, B, C};
}

UavPendParams ModelUavPend::getParams(){
	return p;
}

void ModelUavPend::updateParams(UavPendParams params){
	p = params;
	return;
}

VectorXd ModelUavPend::nonlinStep(const VectorXd x, const VectorXd u, double Ts){

	double phiL = x(3);
	double thetaL = x(4);

	double regularizer = regValue;
	
	if (abs(thetaL) > 85.0/180.0*M_PI && abs(thetaL) < 95.0/180.0*M_PI){
		std::cout << "theta singularity: " << thetaL << "\n";
	}

	MatrixXd M = MatrixXd::Zero(5, 5);
	M.block(0, 0, 3, 3) = (p.m_l + p.m_c) * MatrixXd::Identity(3, 3);
	M.block(0, 3, 3, 2) << 0, -p.l*p.m_l*cos(thetaL),
		p.l*p.m_l*cos(phiL)*cos(thetaL), -p.l*p.m_l*sin(phiL)*sin(thetaL),
		p.l*p.m_l*sin(phiL)*cos(thetaL), p.l*p.m_l*cos(phiL)*sin(thetaL);
	M.block(3, 0, 2, 3) = M.block(0, 3, 3, 2).transpose();
	M(3, 3) = p.l*p.l*p.m_l*cos(thetaL)*cos(thetaL) + regularizer*sin(thetaL)*sin(thetaL);
	M(4, 4) = p.l*p.l*p.m_l;

	double vPhiL = x(8);
	double vThetaL = x(9);
	MatrixXd C = MatrixXd::Zero(5, 5);
	C.block(0, 3, 5, 2) << 0, p.l*p.m_l*vThetaL*sin(thetaL),
		-p.l*p.m_l*(vPhiL*cos(thetaL)*sin(phiL) + vThetaL*cos(phiL)*sin(thetaL)), -p.l*p.m_l*(vPhiL*cos(phiL)*sin(thetaL) + vThetaL*cos(thetaL)*sin(phiL)),
		p.l*p.m_l*(vPhiL*cos(phiL)*cos(thetaL) - vThetaL*sin(phiL)*sin(thetaL)), p.l*p.m_l*(vThetaL*cos(phiL)*cos(thetaL) - vPhiL*sin(phiL)*sin(thetaL)),
		-0.5*p.l*p.l*p.m_l*vThetaL*sin(2*thetaL), -0.5*p.l*p.l*p.m_l*vPhiL*sin(2*thetaL),
		0.5*p.l*p.l*p.m_l*vPhiL*sin(2*thetaL), 0;

	VectorXd G(5);
	G << 0, 0, p.g*(p.m_l + p.m_c), p.l*p.g*p.m_l*cos(thetaL)*sin(phiL), p.l*p.g*p.m_l*cos(phiL)*sin(thetaL);

	MatrixXd D = MatrixXd::Zero(5, 5);
	D.block(3, 0, 2, 5) << 0, cos(phiL), sin(phiL), p.l*cos(thetaL), 0,
		-cos(thetaL), -sin(thetaL)*sin(phiL), cos(phiL)*sin(thetaL), 0, p.l;
	D.block(3, 0, 2, 5) *= p.d;

	double f = u(2);
	double thetaB = x(10);
	double phiB = x(11);
	double psiB = 0;
	VectorXd F(5);
	F << f*(cos(phiB)*cos(psiB)*sin(thetaB) + sin(phiB)*sin(psiB)),
		f*(-cos(psiB)*sin(phiB) + cos(phiB)*sin(thetaB)*sin(psiB)),
		f*cos(thetaB)*cos(phiB),
		0,
		0;

	VectorXd pos(5);
	pos << x(0), x(1), x(2), x(3), x(4);

	VectorXd vel(5);
	vel << x(5), x(6), x(7), x(8), x(9);

	// attitude
	MatrixXd Aatt = MatrixXd::Zero(2,2);
	Aatt << -1/p.tau1, 0,
		0, -1/p.tau2;
	MatrixXd Batt = MatrixXd::Zero(2,2);
	Batt << p.K1/p.tau1, 0,
		0, p.K2/p.tau2;
	auto AattD = MatrixXd::Identity(2, 2) + Ts*Aatt;
	auto BattD = Ts*Batt;
	VectorXd att(2);
	att << x(10), x(11);
	VectorXd uAtt(2);
	uAtt << u(0), u(1);
	VectorXd attOut(2);
	attOut = AattD*att + BattD*uAtt;

	auto acc = M.inverse()*(F - G - C*vel - D*vel);
	auto velOut = vel + Ts*acc;
	auto posOut = pos + Ts*vel;

	VectorXd xOut(12);
	xOut << posOut, velOut, attOut;

	return xOut;
}

void ModelUavPend::setParams(double drag, double reg){
	p.d = drag;
	regValue = reg;
}

}  // namespace pendulum_tracker

}  // namespace pendulum_tracker_plugin