#include <tracker.h>

namespace pendulum_tracker_plugin
{

namespace pendulum_tracker
{

using namespace Eigen;

MpcTracker::MpcTracker(ModelUavPend &model) {

  // get the state-space model
  model_          = model;
  auto [A, B, C_] = model_.getLinModel(TG_TS);
  C = MatrixXd::Identity(N_STATES + N_INPUTS, N_STATES + N_INPUTS);

  // augment the system
  MatrixXd Aaug                               = MatrixXd::Zero(N_STATES + N_INPUTS, N_STATES + N_INPUTS);
  Aaug.block(0, 0, N_STATES, N_STATES)               = A;
  Aaug.block(0, N_STATES, N_STATES, N_INPUTS)        = B;
  Aaug.block(N_STATES, N_STATES, N_INPUTS, N_INPUTS) = MatrixXd::Identity(N_INPUTS, N_INPUTS);

  MatrixXd Baug                        = MatrixXd::Zero(N_STATES + N_INPUTS, N_INPUTS);
  Baug.block(0, 0, N_STATES, N_INPUTS)        = B;
  Baug.block(N_STATES, 0, N_INPUTS, N_INPUTS) = MatrixXd::Identity(N_INPUTS, N_INPUTS);


  // formulate the OCP QP
  Q                   = MatrixXd::Zero(N_STATES + N_INPUTS, N_STATES + N_INPUTS);
  Q.block(0, 0, 3, 3) = 1 * Matrix3d::Identity();

  MatrixXd Qform = C.transpose() * Q * C;

  R = Matrix3d::Zero();

  ocp_qp_solver_plan_t plan;
  plan.qp_solver =
      // FULL_CONDENSING_HPIPM;
      // FULL_CONDENSING_QPOASES;
      // FULL_CONDENSING_QORE;
      // FULL_CONDENSING_DAQP;
      // FULL_CONDENSING_OOQP;
      PARTIAL_CONDENSING_HPIPM;
      // PARTIAL_CONDENSING_QPDUNES;
      // PARTIAL_CONDENSING_OSQP;
      // PARTIAL_CONDENSING_HPMPC;
      // PARTIAL_CONDENSING_OOQP;

  config = ocp_qp_xcond_solver_config_create(plan);
  dims   = ocp_qp_dims_create(TG_PRED_HORIZON);

  int nx   = N_STATES + N_INPUTS;
  int nbx  = 2 + N_INPUTS;
  int nsbx = 2;

  int nu   = N_INPUTS;
  int nbu  = 0;

  int nInitCondConstr = N_STATES;
  int nu_e            = 0;

  for (int i = 0; i < TG_PRED_HORIZON + 1; i++) {
    ocp_qp_dims_set(config, dims, i, "nx", &nx);

    if (i) {
      ocp_qp_dims_set(config, dims, i, "nbx", &nbx);
      ocp_qp_dims_set(config, dims, i, "nsbx", &nsbx);
    } else {
      // initial value for x
      ocp_qp_dims_set(config, dims, i, "nbx", &nInitCondConstr);
    }

    if (i < TG_CTRL_HORIZON) {
      ocp_qp_dims_set(config, dims, i, "nu", &nu);
      ocp_qp_dims_set(config, dims, i, "nbu", &nbu);
    } else {
      // last and first stage has no controls
      ocp_qp_dims_set(config, dims, i, "nu", &nu_e);
    }
  }

  qpIn = ocp_qp_in_create(dims);

  for (int i = 0; i < TG_PRED_HORIZON; i++) {
    ocp_qp_in_set(config, qpIn, i, (char *)"A", Aaug.data());
    ocp_qp_in_set(config, qpIn, i, (char *)"B", Baug.data());
    ocp_qp_in_set(config, qpIn, i, (char *)"Q", Qform.data());
    ocp_qp_in_set(config, qpIn, i, (char *)"R", R.data());
  }

  ocp_qp_in_set(config, qpIn, TG_PRED_HORIZON, (char *)"Q", Qform.data());

  solverDims = ocp_qp_xcond_solver_dims_create_from_ocp_qp_dims(config, dims);

  opts = ocp_qp_xcond_solver_opts_create(config, solverDims);

  // set partial condensing option - they are not defined by default
  if (plan.qp_solver == PARTIAL_CONDENSING_HPIPM
    // || plan.qp_solver == PARTIAL_CONDENSING_HPMPC
    // || plan.qp_solver == PARTIAL_CONDENSING_OOQP
    // || plan.qp_solver == PARTIAL_CONDENSING_OSQP
    // || plan.qp_solver ==  PARTIAL_CONDENSING_QPDUNES
  )
  {
    int N2 = 2;
    ocp_qp_xcond_solver_opts_set(config, (ocp_qp_xcond_solver_opts *)opts, "cond_N", &N2);
  }

  qpOut = ocp_qp_out_create(dims);

  qpSolver = ocp_qp_create(config, solverDims, opts);

}

MpcTracker::~MpcTracker() {
  // free
  ocp_qp_xcond_solver_dims_free(solverDims);
  ocp_qp_dims_free(dims);
  ocp_qp_xcond_solver_config_free(config);
  ocp_qp_xcond_solver_opts_free((ocp_qp_xcond_solver_opts *)opts);
  ocp_qp_in_free(qpIn);
  ocp_qp_out_free(qpOut);
  ocp_qp_solver_destroy(qpSolver);
}

void MpcTracker::updateState(VectorXd stateIn){
  state = stateIn;
  return;
}

std::pair<MatrixXd, MatrixXd> MpcTracker::setReferencePoint(Vector3d refPos, double timeStamp){
  targetTraj.resize(1, 4);

  targetTraj << timeStamp, refPos;

  return getControlAction();
}

std::pair<MatrixXd, MatrixXd> MpcTracker::setReferenceTrajectory(mrs_msgs::TrajectoryReference mrs_traj) {

  int trajLen = mrs_traj.points.size();
  targetTraj.resize(trajLen, 4);

  double timeNow = ros::Time::now().toSec();

  for (int i = 0; i < trajLen; i ++){
    targetTraj.row(i) << timeNow + i*mrs_traj.dt, mrs_traj.points[i].position.x, mrs_traj.points[i].position.y, mrs_traj.points[i].position.z;
  }

  return getControlAction();
}

void MpcTracker::updateReference() {
  double timeNow = ros::Time::now().toSec();

  // indicate the first segment of the trajectory
  int trajSeg = -1;

  for (int i = 0; i <= TG_PRED_HORIZON; i++) {

    double t = timeNow + (double)i * TG_TS;

    if (trajSeg < targetTraj.rows() - 1 && targetTraj(trajSeg + 1, 0) < t) {
      trajSeg++;
    }

    VectorXd ref = VectorXd::Zero(N_STATES);

    if (trajSeg < 0) {
      // just the first segment
      ref.head(3) = state.head(3);
    } else if (!interpolate || trajSeg == targetTraj.rows() - 1) {
      // just the last segment
      ref.head(3) = targetTraj.block(trajSeg, 1, 1, 3).transpose();
    } else {
      // interpolate between segments
      VectorXd refPast = VectorXd::Zero(N_STATES);
      VectorXd refFuture = VectorXd::Zero(N_STATES);

      refPast.head(3) = targetTraj.block(trajSeg, 1, 1, 3).transpose();
      refFuture.head(3) = targetTraj.block(trajSeg + 1, 1, 1, 3).transpose();

      double tPast = targetTraj(trajSeg, 0);
      double tFuture = targetTraj(trajSeg + 1, 0);

      double tElapsed = t - tPast;
      double tPeriod = tFuture - tPast;
      ref = (1 - tElapsed/tPeriod) * refPast + tElapsed/tPeriod * refFuture;
    }

    VectorXd q = VectorXd::Zero(N_STATES);
    q = -C.transpose() * Q * ref;

    ocp_qp_in_set(config, qpIn, i, (char *)"q", q.data());
  }
}

std::pair<MatrixXd, MatrixXd> MpcTracker::getControlAction() {

  ROS_DEBUG("[PendulumTracker/MpcTracker]: Trying to calculate trajectory\n");

  // set the initial condition
  int idxb0[N_STATES + N_INPUTS];
  for (int i = 0; i < N_STATES + N_INPUTS; i++){
    idxb0[i] = i;
  }

  VectorXd stateAug = VectorXd::Zero(N_STATES + N_INPUTS);
  stateAug.head(N_STATES) << state;

  ocp_qp_in_set(config, qpIn, 0, (char *)"idxbx", idxb0);
  ocp_qp_in_set(config, qpIn, 0, (char *)"lbx", stateAug.data());
  ocp_qp_in_set(config, qpIn, 0, (char *)"ubx", stateAug.data());

  if (reqConstrUpdate) {
    updateConstraints();
    reqConstrUpdate = false;
  }
  updateReference();

  // solve the optimization problem
  int acados_return = ocp_qp_solve(qpSolver, qpIn, qpOut);

  if (acados_return != ACADOS_SUCCESS) {
    ROS_INFO("[PendulumTracker/MpcTracker]: qp solver returned status %d. Exiting.\n", acados_return);
    return {state, MatrixXd::Zero(1, N_INPUTS)};
  }

  ROS_DEBUG("[PendulumTracker/MpcTracker]: trajectory successfully generated\n");

  MatrixXd trajectory = MatrixXd::Zero(TG_PRED_HORIZON + 1, N_STATES);
  MatrixXd ctrl = MatrixXd::Zero(TG_PRED_HORIZON + 1, N_INPUTS);
  int             i;
  for (i = 0; i < TG_PRED_HORIZON; i++) {
    // ctrl.row(i) << qpOut->ux[i].pa[N_INPUTS + N_STATES], qpOut->ux[i].pa[N_INPUTS + N_STATES + 1], qpOut->ux[i].pa[N_INPUTS + N_STATES + 2];
    // return increments
    ctrl.row(i) << qpOut->ux[i].pa[0], qpOut->ux[i].pa[1], qpOut->ux[i].pa[2];

    int uOffset = N_INPUTS;

    Vector3d uavPos;
    uavPos << qpOut->ux[i].pa[0 + uOffset], qpOut->ux[i].pa[1 + uOffset], qpOut->ux[i].pa[2 + uOffset];
    Vector3d uavVel;
    uavVel << qpOut->ux[i].pa[5 + uOffset], qpOut->ux[i].pa[6 + uOffset], qpOut->ux[i].pa[7 + uOffset];
    Vector2d uavAcc;
    uavAcc << qpOut->ux[i].pa[10 + uOffset], qpOut->ux[i].pa[11 + uOffset];

    Vector2d loadAtt;
    loadAtt << qpOut->ux[i].pa[3 + uOffset], qpOut->ux[i].pa[4 + uOffset];
    Vector2d loadAttRate;
    loadAttRate << qpOut->ux[i].pa[8 + uOffset], qpOut->ux[i].pa[9 + uOffset];

    trajectory.row(i) << uavPos.transpose(), loadAtt.transpose(), uavVel.transpose(), loadAttRate.transpose(), uavAcc.transpose();

    // check if the generated trajectory is close enough to the goal
    if ((uavPos - targetTraj.block(targetTraj.rows() - 1, 0, 1, 3).transpose()).norm() < TG_REF_ERROR) {
      break;
    }
  }

  return {trajectory.block(0, 0, i, N_STATES), ctrl.block(0, 0, i, N_INPUTS)};
}

void MpcTracker::setParameter(std::string paramName, MatrixXd input) {

  if (paramName == "Q") {
    if (Q == input) {
      return;
    }
    Q     = input;
    input = C.transpose() * input * C;
  } else if (paramName == "R") {
    if (R == input) {
      return;
    }
    R = input;
  } else if (paramName == "bu") {
    if (ubu != input) {
      ubu             = input;
      lbu             = -input;
      reqConstrUpdate = true;
    }
    return;
  }

  for (int i = 0; i <= TG_PRED_HORIZON; i++) {
    ocp_qp_in_set(config, qpIn, i, (char *)paramName.c_str(), input.data());
  }
}

void MpcTracker::setParameter(std::string paramName, double input) {
  if (paramName == "Z") {
    if (velSlackCost != input) {
      velSlackCost    = input;
      reqConstrUpdate = true;
    }
  } else if (paramName == "maxVelHrz") {
    if (maxVelHrz != input) {
      maxVelHrz       = input;
      reqConstrUpdate = true;
    }
  }

  return;
}

void MpcTracker::setParameter(std::string paramName, int input) {
  if (paramName == "interpolate") {
    if (input) {
      interpolate = true;
    } else {
      interpolate = false;
    }
  return;
  }
}

void MpcTracker::updateConstraints() {
  int idxbx[] = {5, 6, 12, 13, 14};

  // bounds on control actions
  VectorXd ubx = VectorXd::Zero(5);
  ubx << maxVelHrz, maxVelHrz, ubu;
  VectorXd lbx = -ubx;

  // softbounds on horizontal velocity
  int             idxsbx[] = {0, 1};
  Vector2d Z        = velSlackCost * Vector2d::Ones();

  for (int i = 0; i <= TG_PRED_HORIZON; i++) {
    if(i){
      ocp_qp_in_set(config, qpIn, i, (char *)"idxbx", idxbx);
      ocp_qp_in_set(config, qpIn, i, (char *)"ubx", ubx.data());
      ocp_qp_in_set(config, qpIn, i, (char *)"lbx", lbx.data());

      ocp_qp_in_set(config, qpIn, i, (char *)"idxs", idxsbx);
      ocp_qp_in_set(config, qpIn, i, (char *)"Zl", Z.data());
      ocp_qp_in_set(config, qpIn, i, (char *)"Zu", Z.data());
    }
  }

  return;
}

}  // namespace pendulum_tracker

}  // namespace pendulum_tracker_plugin
