#ifndef __PENDULUM_TRACKER_H__
#define __PENDULUM_TRACKER_H__

/* includes //{ */

#include <ros/ros.h>
#include <ros/package.h>

#include <mrs_uav_managers/tracker.h>

#include <mrs_lib/param_loader.h>
#include <mrs_lib/mutex.h>
#include <mrs_lib/utils.h>
#include <mrs_lib/attitude_converter.h>
#include <mrs_lib/geometry/cyclic.h>
#include <mrs_lib/publisher_handler.h>
#include <mrs_lib/subscribe_handler.h>

#include <dynamic_reconfigure/server.h>
#include <pendulum_tracker_plugin/pendulum_trackerConfig.h>

#include <eigen3/Eigen/Eigen>

#include <pendulum_controller_plugin/CompleteState.h>
#include <pendulum_controller_plugin/Trajectory.h>
#include <geometry_msgs/PoseArray.h>

#include <tracker.h>
// #include <tracker_extended.h>
#include <model.h>

//}

namespace pendulum_tracker_plugin
{

namespace pendulum_tracker
{

/* //{ class PendulumTracker */

class PendulumTracker : public mrs_uav_managers::Tracker {
public:
  bool initialize(const ros::NodeHandle &nh, std::shared_ptr<mrs_uav_managers::control_manager::CommonHandlers_t> common_handlers,
                  std::shared_ptr<mrs_uav_managers::control_manager::PrivateHandlers_t> private_handlers);

  std::tuple<bool, std::string> activate(const std::optional<mrs_msgs::TrackerCommand> &last_tracker_cmd);
  void                          deactivate(void);
  bool                          resetStatic(void);

  std::optional<mrs_msgs::TrackerCommand>   update(const mrs_msgs::UavState &uav_state, const mrs_uav_managers::Controller::ControlOutput &last_control_output);
  const mrs_msgs::TrackerStatus             getStatus();
  const std_srvs::SetBoolResponse::ConstPtr enableCallbacks(const std_srvs::SetBoolRequest::ConstPtr &cmd);
  const std_srvs::TriggerResponse::ConstPtr switchOdometrySource(const mrs_msgs::UavState &new_uav_state);

  const mrs_msgs::ReferenceSrvResponse::ConstPtr           setReference(const mrs_msgs::ReferenceSrvRequest::ConstPtr &cmd);
  const mrs_msgs::VelocityReferenceSrvResponse::ConstPtr   setVelocityReference(const mrs_msgs::VelocityReferenceSrvRequest::ConstPtr &cmd);
  const mrs_msgs::TrajectoryReferenceSrvResponse::ConstPtr setTrajectoryReference(const mrs_msgs::TrajectoryReferenceSrvRequest::ConstPtr &cmd);

  const mrs_msgs::DynamicsConstraintsSrvResponse::ConstPtr setConstraints(const mrs_msgs::DynamicsConstraintsSrvRequest::ConstPtr &cmd);

  const std_srvs::TriggerResponse::ConstPtr hover(const std_srvs::TriggerRequest::ConstPtr &cmd);
  const std_srvs::TriggerResponse::ConstPtr startTrajectoryTracking(const std_srvs::TriggerRequest::ConstPtr &cmd);
  const std_srvs::TriggerResponse::ConstPtr stopTrajectoryTracking(const std_srvs::TriggerRequest::ConstPtr &cmd);
  const std_srvs::TriggerResponse::ConstPtr resumeTrajectoryTracking(const std_srvs::TriggerRequest::ConstPtr &cmd);
  const std_srvs::TriggerResponse::ConstPtr gotoTrajectoryStart(const std_srvs::TriggerRequest::ConstPtr &cmd);

private:
  void loadParams(mrs_lib::ParamLoader &param_loader);
  void updateMpcParams();

  // update state based on controllers LKF estimate
  void updateMpcState();

  // use only position to update state - assume other states to be zero
  void updateMpcState(const mrs_msgs::UavState & uav_state);
  void visualizeTrajectory(MatrixXd trajectory);

  ros::NodeHandle nh_;

  bool callbacks_enabled_ = true;

  std::string _uav_name_;

  std::shared_ptr<mrs_uav_managers::control_manager::CommonHandlers_t>  common_handlers_;
  std::shared_ptr<mrs_uav_managers::control_manager::PrivateHandlers_t> private_handlers_;

  // | ------------------------ uav state ----------------------- |

  mrs_msgs::UavState uav_state_;
  bool               got_uav_state_ = false;
  std::mutex         mutex_uav_state_;

  // | ------------------ dynamics constriants ------------------ |

  mrs_msgs::DynamicsConstraints constraints_;
  std::mutex                    mutex_constraints_;

  // | ----------------------- goal state ----------------------- |

  double goal_x_       = 0;
  double goal_y_       = 0;
  double goal_z_       = 0;
  double goal_heading_ = 0;

  std::mutex mutex_goal_;
  std::mutex mutex_world_frame_id_;
  std::string world_frame_id_;

  int goal_idx_ = 1;

  // | ---------------- the tracker's inner state --------------- |

  std::atomic<bool> is_initialized_ = false;
  std::atomic<bool> is_active_      = false;

  // | ------------------- for calculating dt ------------------- |

  ros::Time         last_update_time_;
  std::atomic<bool> first_iteration_ = true;

  // | --------------- dynamic reconfigure server --------------- |

  boost::recursive_mutex                                mutex_drs_;
  typedef pendulum_tracker_plugin::pendulum_trackerConfig DrsConfig_t;
  typedef dynamic_reconfigure::Server<DrsConfig_t>      Drs_t;
  boost::shared_ptr<Drs_t>                              drs_;
  void                                                  callbackDrs(pendulum_tracker_plugin::pendulum_trackerConfig &config, uint32_t level);
  DrsConfig_t                                           drs_params_;
  std::mutex                                            mutex_drs_params_;

  // | ----------------------- subscribers ----------------------- |
  mrs_lib::SubscribeHandler<pendulum_controller_plugin::CompleteState> sh_state_;

  // | ----------------------- publishers ----------------------- |
  mrs_lib::PublisherHandler<pendulum_controller_plugin::Trajectory> ph_traj_;
  mrs_lib::PublisherHandler<geometry_msgs::PoseArray> ph_trajViz_;

  double totalMass;
  ModelUavPend model_;
  std::shared_ptr<MpcTracker> tracker;
  // std::shared_ptr<MpcTrackerExtended> trackerExtended;
  MatrixXd traj;
  MatrixXd ctrl;
  MatrixXd ctrlDif;
  mrs_msgs::TrajectoryReference trajRef;
  mrs_msgs::Reference posRef;
  ros::Time timeRefSet;

  int updateI = 0;
  bool trajTracking = false;
};

}  // namespace pendulum_tracker

}  // namespace pendulum_tracker_plugin

#endif