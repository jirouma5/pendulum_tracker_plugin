#ifndef __MODEL_H__
#define __MODEL_H__

#include <eigen3/Eigen/Eigen>

#define N_STATES 12
#define N_INPUTS 3
#define N_OUTPUTS 5

namespace pendulum_tracker_plugin
{

namespace pendulum_tracker
{

using namespace Eigen;

struct UavPendParams{
	// physics parameters
	double d;			// drag coeffitient
	double g;			// gravitational acceleration

	// mechanical parameters
	double m_l;		// weight of the payload
	double m_c;		// weight of the drone
	double l;			// length of the wire
	// double thetaK;
	// double phiK;

	// embedded attitude controller parameters
	double tau1;
	double tau2;
	double K1;
	double K2;
};

class ModelUavPend{
	public:
		ModelUavPend(UavPendParams params = {
			// default params:
			0.1,		// d
			9.81,		// g
			// 0.5,		// m_l
			1.9,		// m_l
			// 2,			// m_c
			3.5,			// m_c
			2,			// l
			0.2,	//0.15,	//0.211,	// tau1
			0.2,	//0.15,	//0.211,	// tau2
			1,	//-0.8,		// K1
			1	//0.8,		// K2
		}):
			p(params)
			{};
		std::tuple<MatrixXd, MatrixXd, MatrixXd> getLinModel(double Ts = 0);
		UavPendParams getParams();
		void updateParams(UavPendParams params);
		void setParams(double drag, double reg);

		VectorXd nonlinStep(const VectorXd x, const VectorXd u, double Ts);
		UavPendParams p;

	private:
		// UavPendParams p;
		double regValue;
};

}  // namespace pendulum_tracker

}  // namespace pendulum_tracker_plugin

#endif