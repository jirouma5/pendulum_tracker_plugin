#ifndef __TRACKER_H__
#define __TRACKER_H__

#include <mrs_uav_managers/tracker.h>

#include <model.h>

#include "acados_c/ocp_qp_interface.h"
#include "acados/utils/print.h"

#include <eigen3/Eigen/Eigen>

#include <mrs_lib/attitude_converter.h>

#include "control_def.h"

// #include <linalg_def.h>

namespace pendulum_tracker_plugin
{

namespace pendulum_tracker
{

using namespace Eigen;

class MpcTracker{
	public:
	MpcTracker(ModelUavPend& model);
	~MpcTracker();

	void updateState(VectorXd stateIn);
	std::pair<MatrixXd, MatrixXd> setReferencePoint(Vector3d refPos, double timeStamp);
	std::pair<MatrixXd, MatrixXd> setReferenceTrajectory(mrs_msgs::TrajectoryReference mrs_traj);
	void setParameter(std::string paramName, MatrixXd input);
	void setParameter(std::string paramName, double input);
	void setParameter(std::string paramName, int input);

	private:
	std::pair<MatrixXd, MatrixXd> getControlAction();
	void updateReference();
	void updateConstraints();

	// acados classes
	ocp_qp_solver* qpSolver;
	ocp_qp_in* qpIn;
	ocp_qp_out* qpOut;
	ocp_qp_xcond_solver_config* config;
	ocp_qp_xcond_solver_dims* solverDims;
	ocp_qp_dims* dims;
	void* opts;

	// initial and reference states
	MatrixXd targetTraj;
	VectorXd state;

	// parameters of the cost function
	Matrix3d R;
	MatrixXd Q;
	MatrixXd C;

	// constraints
		// model constraints
	ModelUavPend model_;

		// control actions constraints
	VectorXd lbu = -0.1*Vector3d::Ones();
	VectorXd ubu = 0.1*Vector3d::Ones();

		// state constraints
	double maxVelHrz = 5;

		// cost of the soft constraints
	double velSlackCost = 100;

	// indicate the need to update constraints
	bool reqConstrUpdate = false;

	// flag for enabling intepolation
	bool interpolate = false;
};

}  // namespace pendulum_tracker

}  // namespace pendulum_tracker_plugin

#endif
